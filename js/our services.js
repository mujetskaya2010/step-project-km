const serviceTab = document.querySelectorAll(".tabs-title");
const serviceContent = document.querySelectorAll(".tabs-text");

const loader = document.createElement("div");

serviceTab.forEach((tab, index) => {
    tab.addEventListener("click", () => {
      serviceTab.forEach((tab) => tab.classList.remove("active"));
      tab.classList.add("active");
  
      serviceContent.forEach((content) => content.classList.remove("active"));
      serviceContent[index].classList.add("active");
    });
  });

